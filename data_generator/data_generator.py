import requests
from random import randrange
import time

JABBA_URL = "http://localhost:8080/api/v1"

for x in range(0, 20000):
    # Pick a random user
    user = randrange(10)
    # Pick a random event
    messageType = "IMPRESSION" if randrange(2) == 0 else "ButtonClick"
    print "%s - %s" %(user,messageType)
    data = "{\"events\":[{\"name\":\"%s\"}]}" %messageType
    headers = {'content-type': 'application/json'}
    result = requests.post("%s/events/applications/testApp/experiments/testExp/users/user-%d" %(JABBA_URL,user), headers = headers, data = data)
    print(result.status_code)
    time.sleep(1)
