package com.intuit.jabba.samzapoc.task;

import org.apache.samza.system.IncomingMessageEnvelope;
import org.apache.samza.system.OutgoingMessageEnvelope;
import org.apache.samza.system.SystemStream;
import org.apache.samza.task.MessageCollector;
import org.apache.samza.task.StreamTask;
import org.apache.samza.task.TaskCoordinator;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class SplitEvents implements StreamTask {
    private static final SystemStream JABBA_IMPRESSIONS = new SystemStream("kafka", "jabba_impressions");
    private static final SystemStream JABBA_ACTIONS = new SystemStream("kafka", "jabba_actions");

    @Override
    public void process(IncomingMessageEnvelope envelope, MessageCollector collector, TaskCoordinator coordinator) throws ParseException, IOException {


        String rawInput = envelope.getMessage().toString();
        HashMap<String, Object> inputMessage = new ObjectMapper().readValue(rawInput, HashMap.class);
        String appName = (String) inputMessage.get("appName");
        String experimentLabel = (String) inputMessage.get("experimentLabel");
        String userId = (String) inputMessage.get("userId");
        ArrayList eventList = (ArrayList) inputMessage.get("events");
        ArrayList impressions = new ArrayList<HashMap<String, String>>();
        ArrayList actions = new ArrayList<HashMap<String,String>>();
        Iterator i = eventList.iterator();

        while(i.hasNext()){
            LinkedHashMap event = (LinkedHashMap) i.next();
            if(((String)event.get("type")).equals("IMPRESSION")){
                impressions.add(event);
            }
            else if(((String)event.get("type")).equals("BINARY_ACTION")){
                actions.add(event);
            }
        }
        if(!impressions.isEmpty()){
            HashMap<String, Object> outgoingImpressions = new HashMap<String,Object>(1);
            outgoingImpressions.put("appName", appName);
            outgoingImpressions.put("experimentLabel", experimentLabel);
            outgoingImpressions.put("userId", userId);
            outgoingImpressions.put("events", impressions);
            System.out.println(outgoingImpressions);
            collector.send(new OutgoingMessageEnvelope(JABBA_IMPRESSIONS,outgoingImpressions));
        }

        if(!actions.isEmpty()){
            HashMap<String, Object> outgoingActions = new HashMap<String,Object>(1);
            outgoingActions.put("appName", appName);
            outgoingActions.put("experimentLabel", experimentLabel);
            outgoingActions.put("userId", userId);
            outgoingActions.put("events", actions);
            System.out.println(outgoingActions);
            collector.send(new OutgoingMessageEnvelope(JABBA_ACTIONS,outgoingActions));
        }

    }
}
