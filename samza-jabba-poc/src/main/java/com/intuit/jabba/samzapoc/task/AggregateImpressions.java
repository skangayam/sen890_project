package com.intuit.jabba.samzapoc.task;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.samza.config.Config;
import org.apache.samza.storage.kv.KeyValueStore;
import org.apache.samza.system.IncomingMessageEnvelope;
import org.apache.samza.system.OutgoingMessageEnvelope;
import org.apache.samza.system.SystemStream;
import org.apache.samza.task.*;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by skangayam on 10/5/14.
 */
public class AggregateImpressions implements StreamTask, InitableTask, WindowableTask {
    private static KeyValueStore<String, String> store;
    private static HashMap<String, HashSet<String>> experiments = new HashMap<String, HashSet<String>>();
    private static HashMap<Integer, Map<String,Object>> impressions_temp = new HashMap<Integer, Map<String, Object>>();
    private static int counter = 0;
    private static final SystemStream JABBA_IMPRESSION_STATS = new SystemStream("kafka", "jabba_impression_stats");

    /**
     * Called once for each message that this StreamTask receives.
     *
     * @param envelope    Contains the received deserialized message and key, and also information regarding the stream and
     *                    partition of which the message was received from.
     * @param collector   Contains the means of sending message envelopes to the output stream. The collector must only
     *                    be used during the current call to the process method; you should not reuse the collector between invocations
     *                    of this method.
     * @param coordinator Manages execution of tasks.
     * @throws Exception Any exception types encountered during the execution of the processing task.
     */
    @Override
    public void process(IncomingMessageEnvelope envelope, MessageCollector collector, TaskCoordinator coordinator)
            throws Exception {
        // append the impressions to an ephemeral data structure that could be flushed out to a data store in each window
        Map<String, Object> impressions = (Map<String, Object>) envelope.getMessage();
        impressions_temp.put(counter, impressions);
        counter++;

        String applicationName = (String)impressions.get("appName");
        String experimentLabel = (String)impressions.get("experimentLabel");
        String userId = (String)impressions.get("userId");
        ArrayList<LinkedHashMap<String,Object>> events = (ArrayList<LinkedHashMap<String, Object>>)impressions
                .get("events");
        String bucketLabel = "";
        String experimentID = "";



        // Get the assignment for the the user for the given experimentLabel and applicationName
        String url = "http://localhost:8080/api/v1/assignments/applications/"+applicationName+"/experiments/"+experimentLabel+"/users/"+userId;
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);
        if(response.getStatusLine().getStatusCode() == 200){
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));
            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            HashMap<String,Object> output = new ObjectMapper().readValue(result.toString(), HashMap.class);
            if(output.get("assignment") != null){
                bucketLabel = (String) output.get("assignment");

            }
        }

        // Get the experimentId for the given experimentLabel and applicationName
        url = "http://localhost:8080/api/v1/applications/appName_demo/experiments/label_demo";
        request = new HttpGet(url);
        response = client.execute(request);
        if(response.getStatusLine().getStatusCode() == 200){
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));
            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            HashMap<String,Object> output = new ObjectMapper().readValue(result.toString(), HashMap.class);
            if(output.get("id") != null){
                experimentID = (String)output.get("id");
            }
        }

        // Update the experiments datastructure to be able to loop through the concise dataset in the window
        HashSet<String> buckets = new HashSet<String>();
        if(experiments.get(experimentID) == null){
            buckets = new HashSet<String>();
        }else {
             buckets = experiments.get(experimentID);
        }
        buckets.add(bucketLabel);
        experiments.put(experimentID, buckets);

        // Determine whether or not this is the first time the user registered an impression
        if(store.get(experimentID+"#"+bucketLabel+"#"+userId) == null){
            // Register the impression of the user for this experimentId|bucket combination in the keystore
            store.put(experimentID+"#"+bucketLabel+"#"+userId,"1");
            if(store.get(experimentID+"#"+bucketLabel+"#"+"distinct_count") == null){
                store.put(experimentID+"#"+bucketLabel+"#"+"distinct_count","1");
            }
            else{
                int distinct = Integer.parseInt(store.get(experimentID+"#"+bucketLabel+"#"+"distinct_count")) + 1;
                store.put(experimentID+"#"+bucketLabel+"#"+"distinct_count",String.valueOf(distinct));
            }
        }

        if(store.get(experimentID+"#"+bucketLabel+"#"+"count") == null){
            store.put(experimentID+"#"+bucketLabel+"#"+"count",String.valueOf(events.size()));
        }
        else{
            int count = Integer.parseInt(store.get(experimentID+"#"+bucketLabel+"#"+"count")) + events.size();
            store.put(experimentID+"#"+bucketLabel+"#"+"count",String.valueOf(count));
        }
    }

    /**
     * Called by TaskRunner each time an implementing task is created.
     *
     * @param config  Allows accessing of fields in the configuration files that this StreamTask is specified in.
     * @param context Allows accessing of contextual data of this StreamTask.
     * @throws Exception Any exception types encountered during the execution of the processing task.
     */
    @Override
    public void init(Config config, TaskContext context) throws Exception {
        this.store = (KeyValueStore<String, String>) context.getStore("impression-stats");
    }

    /**
     * Called by TaskRunner for each implementing task at the end of every specified window.
     *
     * @param collector   Contains the means of sending message envelopes to the output stream. The collector must only
     *                    be used during the current call to the window method; you should not reuse the collector between invocations
     *                    of this method.
     * @param coordinator Manages execution of tasks.
     * @throws Exception Any exception types encountered during the execution of the processing task.
     */
    @Override
    public void window(MessageCollector collector, TaskCoordinator coordinator) throws Exception {

        Iterator it = experiments.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            String experimentId = (String)pairs.getKey();
            HashSet<String> buckets = (HashSet<String>)pairs.getValue();
            HashMap<String, Object> impStats = new HashMap<String,Object>();

            for(String bucketLabel : buckets){
                String count = store.get(experimentId+"#"+bucketLabel+"#"+"count");
                String distinct_count = store.get(experimentId+"#"+bucketLabel+"#"+"distinct_count");
                impStats.put("experimentId",experimentId);
                impStats.put("bucketLabel",bucketLabel);
                impStats.put("count",count);
                impStats.put("distinct_count",distinct_count);
                collector.send(new OutgoingMessageEnvelope(JABBA_IMPRESSION_STATS,impStats));
            }

            experiments = new HashMap<String, HashSet<String>>();
            impressions_temp = new HashMap<Integer, Map<String, Object>>();
            counter = 0;
        }
    }
}
